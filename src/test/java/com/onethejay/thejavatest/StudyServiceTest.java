package com.onethejay.thejavatest;

import com.onethejay.thejavatest.member.MemberService;
import com.onethejay.thejavatest.study.StudyRepository;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class) //MockitoExtension 이 있어야 @Mock 어노테이션을 사용할 수 있다.
public class StudyServiceTest {

    @Mock
    MemberService memberService;

    @Mock
    StudyRepository studyRepository;

}
