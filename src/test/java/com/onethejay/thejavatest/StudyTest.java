package com.onethejay.thejavatest;

import com.onethejay.thejavatest.domain.Study;
import com.onethejay.thejavatest.study.StudyStatus;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;

import java.time.Duration;

import static org.junit.jupiter.api.Assertions.*;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)   //테스트 클래스명이 언더스코어로 되어있으면 공백으로 치환한다.
class StudyTest {

    @DisplayName("스터디 만들기")
    @RepeatedTest(value = 10, name = "{displayName}, {currentRepetition}, {totalRepetitions}")   //반복 횟수, 현재, 총합
    void repeatTest(RepetitionInfo repetitionInfo) {
        System.out.println("test" + repetitionInfo.getCurrentRepetition() + " / " + repetitionInfo.getTotalRepetitions());
    }

    @DisplayName("스터디 만들기")
    @ParameterizedTest(name = "{index} {displayName} message={0}") //파라미터를 인덱스로 참조할수있다.
    @ValueSource(strings = {"날씨가", "많이", "추워지고", "있네요."})
    @NullAndEmptySource //  @EmptySource + @NullSource
    void parameterizedTest(String message) {
        System.out.println(message);
    }

    @FastTest
    void assertTimeoutTest() {        //assertTimeout();
        assertTimeoutPreemptively(Duration.ofMillis(100), () -> {   // Duration에 도달하면 바로 처리
            new Study(10);
            Thread.sleep(300);
        });
    }

    @SlowTest
    void create_new_study() {
        Study study = new Study(-10);
        assertAll(
                () -> assertNotNull(study),
                () -> assertEquals(StudyStatus.DRAFT, study.getStatus(), () -> "스터디를 처음 만들면 상태값이 DRAFT여야 한다."),
                () -> assertTrue(study.getLimit() > 0, "스터디 최대 참석 가능 인원은 0보다 커야 한다.")
        );
    }

    @Test
    void assertThrowsTest() {
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> new Study(-10));
        String message = exception.getMessage();
        assertEquals("limit은 0보다 커야 한다.", message);
    }


    @Test
//    @Disabled
    void create_new_study_again() {
        System.out.println("create1");
    }

    @BeforeAll
    static void beforeAll() {   //static을 꼭 붙여야 한다.
        System.out.println("before all");
    }

    @AfterAll
    static void afterAll() {    //static을 꼭 붙여야 한다.
        System.out.println("after all");
    }

    @BeforeEach
    void beforeEach() {
        System.out.println("before each");
    }

    @AfterEach
    void afterEach() {
        System.out.println("after each");
    }
}