package com.onethejay.thejavatest.study;

public enum StudyStatus {
    DRAFT, STARTED, ENDED
}
